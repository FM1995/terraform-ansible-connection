#### Terraform & Ansible Connection

Following on from Automate Docker using Ansible projects

[Automate Docker Using Ansible - Part 1](https://gitlab.com/FM1995/automate-docker-using-ansible-part-1)

[Automate Docker Using Ansible - Part 2](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2)


So since we configuring the infrastructure using Terraform and the configuration of the server by Ansible there are a few missing links. We have to manually get the IP addreses manually from TF output, then set that in the host file and then set ansible commands.

Now we can automate this from provisioning Terraform to Configuring the servers using Ansible.

#### Lets get started

We will be using this playbook and terraform script in this project demonstration

[deploy-docker-ubuntu-new-user.yaml](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/blob/main/deploy-docker-ubuntu-new-user.yaml?ref_type=heads)

[Main Terraform File for EC2](https://gitlab.com/FM1995/automate-docker-using-ansible-part-2/-/blob/main/terraform-ec2-new-ami/main.tf?ref_type=heads)



In the main.tf we can include the local exec to execute ansible commands, since we are executing the ansible commands locally on our machine

![Image 1](https://gitlab.com/FM1995/terraform-ansible-connection/-/raw/main/Images/Image1.png)

Can then configure the path to the playbook and name

```
provisioner "local_exec" {
  command = "ansible-playbook /home/vboxuser/ansible-docker2/deploy-docker-ubuntu-new-user.yaml"
}
```

![Image 2](https://gitlab.com/FM1995/terraform-ansible-connection/-/raw/main/Images/Image2.png)

![Image 3](https://gitlab.com/FM1995/terraform-ansible-connection/-/raw/main/Images/Image3.png)

The above may not be practical since the playbook relies on variables, so we can set the below as follows so that it can continue to work with the variables

![Image 4](https://gitlab.com/FM1995/terraform-ansible-connection/-/raw/main/Images/Image4.png)

Now we have to think about the hosts ansible will connect when the new ec2 is created

We can then retrieve the public IP of the newly created ec2 instance and overrides the host ip that is present in the ansible folder

```
provisioner "local_exec" {
  working_dir = "/home/vboxuser/ansible-docker2"
  command = "ansible-playbook --inventory ${self.public_ip} deploy-docker-ubuntu-new-user.yaml --private-key ${var.ssh_key_private}"
}
```

![Image 5](https://gitlab.com/FM1995/terraform-ansible-connection/-/raw/main/Images/Image5.png)

Can also define that private key in the ansible command

![Image 6](https://gitlab.com/FM1995/terraform-ansible-connection/-/raw/main/Images/Image6.png)

Can then also set the ssh_key_private in the variables section

![Image 7](https://gitlab.com/FM1995/terraform-ansible-connection/-/raw/main/Images/Image7.png)

And also set the location in the .tfvars file

![Image 8](https://gitlab.com/FM1995/terraform-ansible-connection/-/raw/main/Images/Image8.png)

And then using ubuntu as the user

![Image 9](https://gitlab.com/FM1995/terraform-ansible-connection/-/raw/main/Images/Image9.png)

One more thing is the host that is being referenced in the ansible playbook as the previous hosts is not being referenced, and will then use the new IP can change the referenced hosts which is below

![Image 10](https://gitlab.com/FM1995/terraform-ansible-connection/-/raw/main/Images/Image10.png)

Now ready to run the playbook and getting the below error

![Image 11](https://gitlab.com/FM1995/terraform-ansible-connection/-/raw/main/Images/Image11.png)

Was able to navigate to the fix

Using the below link

https://github.com/plone/ansible.plone_server/issues/86#issuecomment-226457346

Which is a temporary fix

![Image 12](https://gitlab.com/FM1995/terraform-ansible-connection/-/raw/main/Images/Image12.png)

Can then run the below

```
terraform apply –auto-approve
```

![Image 13](https://gitlab.com/FM1995/terraform-ansible-connection/-/raw/main/Images/Image13.png)

![Image 14](https://gitlab.com/FM1995/terraform-ansible-connection/-/raw/main/Images/Image14.png)

Can then ssh into the ec2-instance and display the containers from the docker compose running

![Image 15](https://gitlab.com/FM1995/terraform-ansible-connection/-/raw/main/Images/Image15.png)

Sometimes there might be a timing issue, where it will attempt to run the ansible playbook before the ec2 instance is ready

So to resolve this we can use the below documentation

https://docs.ansible.com/ansible/latest/collections/ansible/builtin/wait_for_module.html

Can use any of these


![Image 16](https://gitlab.com/FM1995/terraform-ansible-connection/-/raw/main/Images/Image16.png)

The play contains a single task.
name: Ensure ssh port open: This is the name of the task, indicating its purpose.

wait_for: This uses the wait_for module of Ansible, which is designed to wait for a specific condition before continuing with the next task.

port: 22: This specifies that the task should wait for port 22 (the default SSH port) to be open.

delay: 10: This adds a delay of 10 seconds before starting to check the port.

timeout: 100: Sets a timeout of 100 seconds. If the condition is not met within this time, the task will fail.

search_regex: OpenSSH: This option is used to look for a specific pattern in the response. In this case, it's looking for the term "OpenSSH" which is a common SSH implementation.

host: '{{ (ansible_ssh_host|default(ansible_host))|default(inventory_hostname) }}': This complex Jinja2 expression determines the host to check. It tries to use ansible_ssh_host, if not set, then 

ansible_host, and if that's not set, it defaults to the inventory hostname.

Vars:
ansible_connection: local: This variable sets the connection type to local, meaning the task will be executed on the local machine running the playbook, not on a remote host.

![Image 17](https://gitlab.com/FM1995/terraform-ansible-connection/-/raw/main/Images/Image17.png)

Another way is using null resource which we can use the provisioning on the ec2 servers as a seperate task to unrelated resources like ec2

Using the below link

https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource

Can configure it like the below

```
resource "null_resource" "configure_server" {
  provisioner "local-exec" {
    working_dir = "/home/vboxuser/ansible-docker2"
    command = "ansible-playbook --inventory ${self.public_ip}, --private-key ${var.ssh_key}"
  }
}
```


![Image 18](https://gitlab.com/FM1995/terraform-ansible-connection/-/raw/main/Images/Image18.png)

Can then reference the IP using the resource name

```
${aws_instance.myapp-server.public_ip}
```

```
resource "null_resource" "configure_server" {
  provisioner "local-exec" {
    working_dir = "/home/vboxuser/ansible-docker2"
    command = "ansible-playbook --inventory ${aws_instance.myapp-server.public_ip}, --private-key ${var.ssh_key}"
  }
}
```

![Image 19](https://gitlab.com/FM1995/terraform-ansible-connection/-/raw/main/Images/Image19.png)

Can also include a trigger so that when a new IP address from an ec2 instance is configured it will apply the ansible command

```
triggers = {
  trigger = aws_instance.myapp-server.public_ip
}
```

![Image 20](https://gitlab.com/FM1995/terraform-ansible-connection/-/raw/main/Images/Image20.png)

Can then run terraform apply

And can see null resources got executed

![Image 21](https://gitlab.com/FM1995/terraform-ansible-connection/-/raw/main/Images/Image21.png)

And then going in to the server, can see the containers

![Image 22](https://gitlab.com/FM1995/terraform-ansible-connection/-/raw/main/Images/Image22.png)










